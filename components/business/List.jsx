import Input from "../ui/Input"
import { FaTrash } from "react-icons/fa"
import { useContext, useState } from "react"
import { context } from "../../pages"

const List = (props) => {
  const { name, checked, onChange, description, ...otherProps } = props

  const { setData, data, currentlist, setOpenModal, setTypeAction } =
    useContext(context)
  const [isShown, setIsShown] = useState(false)

  const handleClickDeleteList = () => {
    data.todoList.filter((x) => x.id === currentlist)[0].todo.splice(name, 1)
    setData((prevData) => ({
      ...prevData,
      todoList: data.todoList,
    }))
  }

  const handleClickEditList = () => {
    setOpenModal({
      editList: name,
      open: true,
    })
    setTypeAction("EDITLIST")
  }

  return (
    <div
      onMouseEnter={() => setIsShown(true)}
      onMouseLeave={() => setIsShown(false)}
      className="flex basis-full p-1 border-b-2 justify-between"
      {...otherProps}
    >
      <div className="flex p-1">
        <Input
          name={name}
          type="checkbox"
          className="mr-1"
          checked={checked}
          onChange={onChange}
        />
        <h1 onClick={handleClickEditList}>{description}</h1>
      </div>
      {isShown && (
        <div>
          <FaTrash onClick={handleClickDeleteList} />
        </div>
      )}
    </div>
  )
}

export default List
