import * as yup from "yup"
import { Formik, Form } from "formik"
import { useCallback } from "react"
import FormField from "../ui/FormField"
import Button from "../ui/Button"
import { context } from "../../pages"
import { useContext } from "react"
const Modal = (props) => {
  const { type, editList } = props
  const { setOpenModal, setData, data, setCurrentList, currentlist } =
    useContext(context)

  const initialValues = {
    description: "",
  }
  const validationSchema = yup.object().shape({
    description: yup.string().required(),
  })

  const addList = useCallback(
    (description) => {
      data.todoList.push({
        id: data.lastInsertedId + 1,
        list: description,
        todo: [],
        countDone: 0,
      })
      setData((prevData) => ({
        ...prevData,
        lastInsertedId: data.lastInsertedId + 1,
        todoList: data.todoList,
      }))
      setOpenModal((prevData) => ({
        ...prevData,
        open: false,
      }))
      setCurrentList(data.lastInsertedId + 1)
    },
    [setData, data.lastInsertedId, data.todoList, setOpenModal, setCurrentList]
  )

  const addTodo = useCallback(
    (description) => {
      const index = data.todoList.findIndex((list) => list.id === currentlist)
      data.todoList[index].todo.push({ description: description, done: false })
      setData((prevData) => ({
        ...prevData,
        todoList: data.todoList,
      }))
      setOpenModal((prevData) => ({
        ...prevData,
        open: false,
      }))
    },
    [currentlist, setData, data.todoList, setOpenModal]
  )
  const editTodo = useCallback(
    (description) => {
      const index = data.todoList.findIndex((todo) => todo.id === currentlist)
      data.todoList[index].list = description
      setData((prevData) => ({
        ...prevData,
        todoList: data.todoList,
      }))
      setOpenModal((prevData) => ({
        ...prevData,
        open: false,
      }))
    },
    [currentlist, setData, data.todoList, setOpenModal]
  )

  const editcList = useCallback(
    (description) => {
      data.todoList.filter((x) => x.id == currentlist)[0].todo[
        editList
      ].description = description
      setData((prevData) => ({
        ...prevData,
        todoList: data.todoList,
      }))
      setOpenModal({
        open: false,
      })
    },
    [currentlist, setData, data.todoList, editList, setOpenModal]
  )

  const handleSubmit = useCallback(
    (values, { resetForm }) => {
      switch (type) {
        case "EDITTODO":
          editTodo(values.description)

          break

        case "ADDTODO":
          addTodo(values.description)

          break

        case "ADDLIST":
          addList(values.description)

          break

        case "EDITLIST":
          editcList(values.description)

          break

        default:
          break
      }

      resetForm()
    },
    [editTodo, addList, addTodo, editcList, type]
  )

  const handleClose = useCallback(() => {
    setOpenModal({
      open: false,
    })
  }, [setOpenModal])

  return (
    <div
      className="modal fixed top-0 right-0  bottom-0	left-0 min-h-full min-w-full bg-white"
      id="exampleModalFullscreen"
      tabIndex="-1"
      aria-labelledby="exampleModalFullscreenLabel"
      aria-hidden="true"
    >
      <div className="modal-content">
        <div className="modal-header">
          <h4 className="modal-title text-2xl font-bold">
            {type === "ADDLIST"
              ? "Create a new list"
              : type === "ADDTODO"
              ? "Create a todo"
              : type === "EDITTODO"
              ? "Modify a todo"
              : "Modify a list"}
          </h4>
        </div>
        <div className="modal-body">
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}
          >
            <Form className="flex flex-col">
              <FormField
                label="Description"
                name="description"
                placeholder="Enter your list"
              />
              <Button
                className="mt-4 bg-blue-600	text-white p-2 rounded-lg fixed bottom-10 right-10"
                type="submit"
              >
                Submit
              </Button>
            </Form>
          </Formik>
          <Button
            className="mt-4 	text-black p-2 rounded-lg fixed bottom-10 right-40 font-bold"
            onClick={handleClose}
          >
            Cancel
          </Button>
        </div>
      </div>
    </div>
  )
}

export default Modal
