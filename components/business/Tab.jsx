import Li from "../ui/Li"
import Span from "../ui/Span"
import Button from "../ui/Button"
import Progress from "../ui/Progress"
import { context } from "../../pages"
import { useContext } from "react"

const Tab = (props) => {
  const { title, countList, countDone, id, ...otherProps } = props
  const { currentlist } = useContext(context)

  return (
    <Li
      className=" flex flex-col rounded-t-lg border-x-2 border-t-2"
      {...otherProps}
    >
      <Button className="inline-flex p-1 justify-evenly " name={title}>
        {title}
        <div>
          <Span className="ml-2  bg-emerald-400 px-2 rounded-full text-black z-0">
            {countDone}
          </Span>
          <Span className=" bg-sky-600 -ml-2 px-2 rounded-full text-black z-10">
            {countList}
          </Span>
        </div>
      </Button>

      {id == currentlist && (
        <Progress max={countList} value={countDone} className="max-h-1 ">
          10
        </Progress>
      )}
    </Li>
  )
}

export default Tab
