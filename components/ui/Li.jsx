import classNames from "classnames"

const Li = (props) => {
  const { className, children, ...otherProps } = props

  return (
    <li className={classNames(className)} {...otherProps}>
      {children}
    </li>
  )
}

export default Li
