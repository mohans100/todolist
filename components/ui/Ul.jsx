const Ul = (props) => {
  const { children, ...otherProps } = props

  return (
    <ul
      className="relative flex flex-nowrap text-sm font-medium text-center text-gray-500 border-b border-gray-200 dark:border-gray-700 dark:text-gray-400e overflow-auto"
      {...otherProps}
    >
      {children}
    </ul>
  )
}

export default Ul
