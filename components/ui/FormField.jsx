import { Field } from "formik"
import Label from "./Label"

const FormField = (props) => {
  const { label, ...inputProps } = props

  return (
    <Label className="p-2 flex flex-col gap-2">
      <span className="font-semibold">{label}</span>
      <Field className="border-2 px-2 p-1" {...inputProps} />
    </Label>
  )
}

export default FormField
