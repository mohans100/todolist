import classNames from "classnames"

const Progress = (props) => {
  const { className, children, ...otherProps } = props

  return (
    <progress className={classNames(className)} {...otherProps}>
      {children}
    </progress>
  )
}

export default Progress
