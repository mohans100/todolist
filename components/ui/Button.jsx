import classNames from "classnames"

const Button = (props) => {
  const { className, children, ...otherProps } = props

  return (
    <button className={classNames(className)} {...otherProps}>
      {children}
    </button>
  )
}

export default Button
