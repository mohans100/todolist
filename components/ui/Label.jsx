import classNames from "classnames"

const Label = (props) => {
  const { className, children, ...otherProps } = props

  return (
    <label className={classNames("font-bold", className)} {...otherProps}>
      {children}
    </label>
  )
}

export default Label
