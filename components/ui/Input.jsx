import classNames from "classnames"

const Input = (props) => {
  const { className, name, type, ...otherProps } = props

  return (
    <input
      className={classNames("border-2	", className)}
      name={name}
      type={type}
      {...otherProps}
    />
  )
}

export default Input
