import classNames from "classnames"
const Span = (props) => {
  const { className, children, ...otherProps } = props

  return (
    <span className={classNames(className, "font-bold")} {...otherProps}>
      {children}
    </span>
  )
}

export default Span
