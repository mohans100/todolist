import Ul from "../components/ui/Ul"
import { FaPlus, FaTrash, FaRegEdit, FaRegCheckCircle } from "react-icons/fa"
import Button from "../components/ui/Button"
import Modal from "../components/business/Modal"
import { createContext, useCallback, useState } from "react"
import List from "../components/business/List"
import Tab from "../components/business/Tab"

export const context = createContext()

export default function Home() {
  const [openModal, setOpenModal] = useState({
    open: false,
  })
  const [data, setData] = useState({
    lastInsertedId: 0,
    todoList: [],
  })
  const [currentlist, setCurrentList] = useState(0)
  const [typeAction, setTypeAction] = useState("")
  const [filter, setFilter] = useState(false)

  const handleClick = useCallback((e, id) => {
    setCurrentList(id)
  }, [])

  const handleClickAddList = useCallback(() => {
    setOpenModal({
      open: true,
    })
    setTypeAction("ADDLIST")
  }, [])

  const handleClickAddTodo = useCallback(() => {
    setOpenModal({
      open: true,
    })
    setTypeAction("ADDTODO")
  }, [])

  const handleClickRemoveTodo = useCallback(() => {
    const index = data.todoList.findIndex((todo) => todo.id === currentlist)

    if (data.todoList[index - 1] != null) {
      setCurrentList(data.todoList[index - 1].id)
    } else {
      setCurrentList(data.todoList[0].id)
    }

    data.todoList.splice(index, 1)
    setData((prevData) => ({
      ...prevData,
      todoList: data.todoList,
    }))
  }, [currentlist, data.todoList])

  const handleClickEditTdo = useCallback(() => {
    setOpenModal({
      open: true,
    })
    setTypeAction("EDITTODO")
  }, [])

  const handleDoneList = useCallback(
    (e) => {
      const index = data.todoList.findIndex((todo) => todo.id === currentlist)
      data.todoList[index].todo[parseInt(e.target.name)].done = e.target.checked

      if (e.target.checked) {
        data.todoList[index].countDone += 1
      } else {
        data.todoList[index].countDone -= 1
      }

      setData((prevData) => ({
        ...prevData,
        todoList: data.todoList,
      }))
    },
    [data.todoList, currentlist]
  )

  const handleClickFilter = useCallback(() => {
    setFilter(!filter)
  }, [filter])

  return (
    <context.Provider
      value={{
        setOpenModal,
        setData,
        data,
        setCurrentList,
        currentlist,
        setTypeAction,
      }}
    >
      <div className="flex-col">
        <div className="sticky top-0">
          <Ul>
            {Array.isArray(data.todoList)
              ? data.todoList.map((element) => (
                  <Tab
                    title={element.list}
                    key={element.id}
                    id={element.id}
                    countList={element?.todo.length}
                    countDone={element?.countDone}
                    onClick={(e) => handleClick(e, element.id)}
                  />
                ))
              : null}

            <Button
              className=" ml-3  inline-flex px-3 py-1 justify-evenly rounded-t-lg  border-x-2 border-t-2"
              onClick={handleClickAddList}
            >
              +
            </Button>
          </Ul>
          <div className="flex justify-between border-b-2 p-2">
            <div className="flex p-1">
              <FaPlus className="mr-2" onClick={handleClickAddTodo} />
              <FaTrash className="mr-2" onClick={handleClickRemoveTodo} />
              <FaRegEdit className="mr-2" onClick={handleClickEditTdo} />
            </div>
            <div>
              <FaRegCheckCircle onClick={handleClickFilter} />
            </div>
          </div>
        </div>

        <div className=" flex flex-col">
          {data.todoList.filter((x) => x.id === currentlist)[0]?.todo && filter
            ? data.todoList
                .filter((x) => x.id === currentlist)[0]
                .todo.filter((x) => x.done == false)
                .map((element, index) => (
                  <List
                    name={index}
                    key={index}
                    checked={element.done}
                    onChange={(e) => handleDoneList(e)}
                    description={element.description}
                  />
                ))
            : data.todoList.filter((x) => x.id === currentlist)[0]?.todo
            ? data.todoList
                .filter((x) => x.id === currentlist)[0]
                .todo.map((element, index) => (
                  <List
                    name={index}
                    key={index}
                    checked={element.done}
                    onChange={(e) => handleDoneList(e)}
                    description={element.description}
                  />
                ))
            : null}
        </div>
        {openModal.open && (
          <Modal
            editList={openModal.editList}
            edit={openModal.editTodo}
            type={typeAction}
          />
        )}
      </div>
    </context.Provider>
  )
}
